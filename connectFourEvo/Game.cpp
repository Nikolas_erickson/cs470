#include <iostream>
#include <vector>

#include "Game.h"

using namespace std;

Game::Game()
{
   gameOver = false;
   currentPlayer = 0;
}
Game::Game(Player * player1, Player * player2)
{
   gameOver = false;
   currentPlayer = 0;
   players.push_back(player1);
   players.push_back(player2);
}

int Game::play(bool quiet)
{
   while(!gameOver)
   {
      if(!quiet)
      {
         printBoard();
         printCurrentPlayer();
      }
      makeMove();
      updateGameStatus();
      switchPlayer();
   }
   switchPlayer(); // switch player back to winning player if game ends
   if(!quiet) printBoard();
   return printWinner();
}


void Game::switchPlayer()
{
   currentPlayer = (currentPlayer + 1) % players.size();
}

void Game::printBoard()
{
   board.print();
}

int Game::printWinner()
{
   int result = 0;
   if(board.checkWin(players[currentPlayer]->getPiece()))
   {
      result = currentPlayer + 1;
   }
   else
   {
      printTie();
   }
   return result;
}

void Game::printTie()
{
   if(board.isFull())
   {
      std::cout << "It's a tie!" << std::endl;
   }
}

void Game::printCurrentPlayer()
{
   std::cout << "Player " << players[currentPlayer]->getPiece() << "'s turn." << std::endl;
}

void Game::makeMove()
{
   int column;
   column = players[currentPlayer]->getMove(board);
   board.makeMove(column, players[currentPlayer]->getPiece());
}

void Game::updateGameStatus()
{
   if(board.checkWin(players[currentPlayer]->getPiece()))
   {
      gameOver = true;
      cout << "\u001b[44m" << players[currentPlayer]->getPiece() << " wins!\u001b[40m" << endl;
      players[currentPlayer]->printWeights();
      cout << endl;
   }
   else if(board.isFull())
   {
      gameOver = true;
      cout << "\u001b[44mIt's a tie!\u001b[40m" << endl;
   }
}

