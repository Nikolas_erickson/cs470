#ifndef BOARDCONTIG_H
#define BOARDCONTIG_H

#include <string>

using namespace std;

class Board
{
public:
   Board();
   Board(const Board & other);
   ~Board();
   void print() const;
   bool makeMove(int column, char piece);
   bool isFull() const;
   bool checkWin(char piece) const;
   bool isValidMove(int column) const;

   void loadBoard(const char * filename);

   int evaluate(char piece, int * pointValues) const;
   string getHash() const;
   string getHashMirror() const;

private:
   bool checkVertical(int row, int column, char piece) const;
   bool checkHorizontal(int row, int column, char piece) const;
   bool checkDiagonal(int row, int column, char piece) const;


   char * grid;
};

#endif