#ifndef PLAYER_H
#define PLAYER_H

#include "BoardContig.h"

class Player
{
public:
   Player(char piece, bool isHuman = true);
   Player(char piece, bool isHuman, int * weights);
   ~Player();
   char getPiece() const;
   bool isHuman() const;
   int getMove(Board board) const;
   void printWeights() const;

   void setPointValues(int * weights);
   bool getPointValues(int * weights) const;
private:
   char m_piece;
   bool m_isHuman;
   int * m_weights;

   int getBestMove(Board board) const;
};

#endif