#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <vector>

#include "BoardContig.h"
#include "Player.h"

class Game
{
public:
   Game();
   Game(Player * player1, Player * player2);
   int play(bool quiet = false);

private:
   Board board;
   std::vector<Player *> players;
   int currentPlayer;
   bool gameOver;

   void switchPlayer();
   void printBoard();
   int printWinner();
   void printTie();
   void printCurrentPlayer();
   void makeMove();
   void updateGameStatus();
};

#endif