#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include "Game.h"
#include "Player.h"


#define DEBUG_PRINT(x) if(g_debugFlag) { cout << x << endl; }

bool g_debugFlag = false;
static int g_populationSize = 30;


uint64_t millisM()
{
   uint64_t ms = chrono::duration_cast<chrono::milliseconds>(
      chrono::high_resolution_clock::now().time_since_epoch())
      .count();
   return ms;
}



inline bool exists(const char * filename)
{
   struct stat buffer;
   return (stat(filename, &buffer) == 0);
}

void generatePlayer(vector<int *> & pop, vector<int> & fitness)
{
   // generate population
   for(int i = 0; i < g_populationSize - 1; i++)
   {
      int * dna = new int[4];
      dna[0] = (rand() % 200) + 1;
      for(int j = 1; j < 4; j++)
      {
         dna[j] = (rand() % 10) + 1;
      }
      pop.push_back(dna);
      fitness.push_back(0);
   }
   pop.push_back(new int[4] {100, 3, 2, 1});
   fitness.push_back(0);
}

void loadPlayer(vector<int *> & pop, vector<int> & fitness, const char * filename)
{
   //if file exists
   if(exists(filename))
   {
      //open file
      FILE * file = fopen(filename, "r");
      //load contents into c string
      char line[256];
      for(int i = 0; i < g_populationSize; i++)
      {
         fgets(line, 255, file);
         cout << line << endl;
         //parse c string into vector
         int * dna = new int[4];
         dna[0] = atoi(strtok(line, ","));
         for(int i = 1; i < 4; i++)
         {
            dna[i] = atoi(strtok(NULL, ","));
         }
         cout << "DNA: " << dna[0] << " " << dna[1] << " " << dna[2] << " " << dna[3] << endl;
         int fitnessIn = atoi(strtok(NULL, ","));
         pop.push_back(dna);
         fitness.push_back(fitnessIn);
      }
      fclose(file);
   }
   else
   {
      generatePlayer(pop, fitness);
   }
}

void savePlayer(vector<int *> & pop, vector<int> & fitness, const char * filename)
{
   //open file
   FILE * file = fopen(filename, "w");
   //write to file
   for(int i = 0; i < pop.size() && i < fitness.size(); i++)
   {
      fprintf(file, "%d,%d,%d,%d,%d\n", pop[i][0], pop[i][1], pop[i][2], pop[i][3], fitness[i]);
   }
   fclose(file);
   cout << "Saved to file" << endl;
}

void printTimes(uint64_t timer, vector<int> * times)
{
   uint64_t time = millisM() - timer;
   times->push_back(time);
   std::cout << "Time: " << time << "ms" << std::endl;
   uint64_t total = 0;
   for(int i = 0; i < times->size(); i++)
   {
      total += times->at(i);
   }
   cout << "Average time: " << total / times->size() << "ms" << endl;
}



int main(int argc, char * argv[])
{
   vector<int> times;
   srand(time(NULL));
   vector<int *> pop;
   vector<int> fitness;


   // handle any commandline flags
   if(argc > 1)
   {
      int argsLeft = argc - 1;
      while(argsLeft > 0)
      {
         if(strcmp(argv[argc - argsLeft], "-p") == 0)
         {
            if(argsLeft < 2)
            {
               cout << "Invalid argument: " << argv[argc - argsLeft] << endl;
               cout << "Usage: -p <population size>" << endl;
               argsLeft--;
               continue;
            }
            g_populationSize = atoi(argv[argc - argsLeft + 1]);
            argsLeft -= 2;
         }
         else if(strcmp(argv[argc - argsLeft], "-f") == 0)
         {
            if(argsLeft < 2)
            {
               cout << "Invalid argument: " << argv[argc - argsLeft] << endl;
               cout << "Usage: -f <filename>" << endl;
               argsLeft--;
               continue;
            }
            char * filename = argv[argc - argsLeft + 1];
            loadPlayer(pop, fitness, filename);
            argsLeft -= 2;
         }
         else if(strcmp(argv[argc - argsLeft], "-h") == 0)
         {
            cout << "Usage: -p <population size> -f <filename>" << endl;
            argsLeft--;
         }
         else if(strcmp(argv[argc - argsLeft], "-v") == 0)
         {
            g_debugFlag = true;
            argsLeft--;
         }

         else
         {
            cout << "Invalid argument: " << argv[argc - argsLeft] << endl;
            cout << "Usage: -p <population size> -f <filename>" << endl;
            argsLeft--;
         }
      }
   }

   if(pop.size() == 0)
   {
      generatePlayer(pop, fitness);
   }


   // start the game
   int generation = 0;
   while(true)
   {
      // start generation
      generation++;
      cout << "Generation: " << generation << endl;

      // reset fitness
      for(int i = 0; i < fitness.size(); i++)
      {
         fitness[i] = 0;
      }

      //create new generation
      int genSize = pop.size();
      for(int i = 0; i < genSize; i++)
      {
         int * newDna = new int[4];
         newDna[0] = pop[i][0] + rand() % 101 - 50;
         for(int j = 1; j < 4; j++)
         {
            newDna[j] = pop[i][j] + rand() % 7 - 3;
         }
         pop.push_back(newDna);
         fitness.push_back(0);
      }

      // display all players
      for(int i = 0; i < pop.size(); i++)
      {
         cout << "Player " << i << ": ";
         for(int j = 0; j < 4; j++)
         {
            cout << pop[i][j] << " ";
         }
         cout << "Fitness: " << fitness[i] << endl;
      }

      //play games
      int round = 0;
      for(int i = 0; i < pop.size() - 1; i++)
      {
         for(int j = i + 1; j < pop.size(); j++)
         {
            round++;
            // play one round with each player as O
            Player computer1('O', false, pop[i]);
            Player computer2('X', false, pop[j]);
            {

               uint64_t timer = millisM();
               cout << "Round " << round << ": Player " << i << " vs Player " << j << " " << endl;
               Game game(&computer1, &computer2);
               DEBUG_PRINT("Game created");
               int result = game.play(true);
               if(result == 1)
               {
                  fitness[i]++;
               }
               else if(result == 2)
               {
                  fitness[j]++;
               }
               // display results
               cout << "Winner is Player " << result << endl;
               printTimes(timer, &times);
            }
            // switch sides
            {
               uint64_t timer = millisM();
               cout << "Round " << round << ": Player " << j << " vs Player " << i << " ";
               Game game(&computer2, &computer1);
               int result = game.play(true);
               if(result == 1)
               {
                  fitness[i]++;
               }
               else if(result == 2)
               {
                  fitness[j]++;
               }
               // display results
               cout << "Winner is Player " << result << endl;
               printTimes(timer, &times);
            }
         }
      }

      // sort them and print results
      vector<int *> newPop;
      vector<int> newFitness;
      while(!pop.empty())
      {
         // put strongest play into new list
         int maxIndex = 0;
         for(int i = 1; i < pop.size(); i++)
         {
            if(fitness[i] > fitness[maxIndex])
            {
               maxIndex = i;
            }
         }
         newPop.push_back(pop[maxIndex]);
         newFitness.push_back(fitness[maxIndex]);
         pop.erase(pop.begin() + maxIndex);
         fitness.erase(fitness.begin() + maxIndex);
      }

      cout << "Results:" << endl;
      for(int i = 0; i < newPop.size(); i++)
      {
         cout << "Player " << i << ": " << newPop[i][0] << " " << newPop[i][1] << " " << newPop[i][2] << " " << newPop[i][3] << " Fitness: " << newFitness[i] << endl;
      }

      // save all players to file, in case the next time I want to  load a larger population
      savePlayer(pop, fitness, "genPop.txt");



      int newPopSize = 0;
      while(newPopSize < g_populationSize)
      {
         newPop.push_back(newPop[newPopSize]);
         newFitness.push_back(newFitness[newPopSize]);
         newPopSize++;
      }
      // free the unused population (the culled)
      while(newPopSize < newPop.size())
      {
         delete[] newPop[newPopSize];
         newPopSize++;
      }
   }

   return 0;
}