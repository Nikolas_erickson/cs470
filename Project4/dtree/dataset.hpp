#ifndef STD_DATASET_HPP
#define STD_DATASET_HPP

#include <string>
//#include <unordered_map>
#include <vector>

#include "data_node.hpp"

namespace std {

	class Dataset {
	public:
		Dataset(vector<vector<string> *> * data);

		vector<Dataset *> split(uint column); // split dataset by unique values in column

		Dataset randomSelection(uint size); // select random sample of data
		Dataset slice(uint start, uint size); // select a slice of data

		bool hasSameResult(); // returns true if all data has the same result
		uint getSize() { return _dataNodes.size(); } // returns length of dataset
		bool isFullySorted(); // returns true if data is sorted by all columns

		vector<bool> getSortedBy() { return _sortedBy; } // returns sortedBy
		bool isSortedBy(uint column) { return _sortedBy[column]; } // returns true if data is sorted by column
		int getMajorityResult(); // returns majority result

		vector<DataNode *> getDataNodes() { return _dataNodes; } // returns dataNodes
		DataNode * getDataNode(uint index) { return _dataNodes[index]; } // returns dataNode at index
		uint getUniqueValueCount(uint column) { return _uniqueValues[column]->size(); } // returns number of unique values in column
		uint getUniqueResultCount() { return _uniqueValues.back()->size(); } // returns number of unique results

		void printRaw();
		void print();

		string getUniqueValue(int index, uint column); // get unique value from uniqueValues

		int getResultComlunIndex() { return _uniqueValues.size() - 1; }


	private:
		Dataset(); // private constructor for randomSelection

		vector<DataNode *> _dataNodes; // each node represents a row of data
		vector<vector<string> *> _uniqueValues; // unique values for each column
		vector<bool> _sortedBy; // true if data has been sorted by the column
		uint _size; // number of rows in dataset

		// helper functions for creating dataset
		int getUniqueValueIndex(string value, uint column); // get index of unique value in uniqueValues, if not found: add it
	};

} // namespace std

#endif  // STD_DATASET_HPP
