#include <math.h>

#include "d_tree_node.hpp"


#define DEBUG 0
#define debug_print(...) if (DEBUG) fprintf(stderr, __VA_ARGS__)

namespace std {

   DTreeNode::DTreeNode(Dataset * dataset)
   {
      debug_print("DTreeNode::DTreeNode created\n");
      if(DEBUG)
      {
         dataset->print();
      }
      if(dataset->getSize() == 0)
      {
         // this should not happen
         fprintf(stderr, "DTreeNode::DTreeNode: data is empty\n");
         _isResult = true;
         _result = 0;
         return;
      }

      // initialize training data
      _dataset = dataset;
      _dataNodes = dataset->getDataNodes();

      // if the training data all has the same result, this is a result node
      _isResult = _dataset->hasSameResult() || _dataset->isFullySorted();
      debug_print("DTreeNode::DTreeNode: _dataset->hasSameResult() is %d\n", _dataset->hasSameResult());
      debug_print("DTreeNode::DTreeNode: _dataset->isFullySorted() is %d\n", _dataset->isFullySorted());

      // if this is a result node, we're done
      if(_isResult)
      {
         _result = _dataset->getMajorityResult();
         debug_print("DTreeNode::DTreeNode: result node, raw result is %d, result is %s\n", _result, _dataset->getUniqueValue(_result, _dataset->getResultComlunIndex()).c_str());
         return;
      }

      _predictorIndex = getNextPredictor();
      vector<Dataset *> splits = _dataset->split(_predictorIndex);

      for(uint i = 0; i < splits.size(); i++)
      {
         debug_print("DTreeNode::DTreeNode: split %d\n", i);
      }

      // create child nodes
      for(uint i = 0; i < splits.size(); i++)
      {
         _results.push_back(new DTreeNode(splits[i]));
         _isResult = false;
      }
   }

   // returns next node based on dataNode
   DTreeNode * DTreeNode::getNextNode(DataNode * dataNode)
   {
      if(_isResult)
      {
         return this;
      }
      int value = dataNode->getPredictor(_predictorIndex);
      return _results[value];
   }

   int DTreeNode::getNextPredictor()
   {
      // find the best predictor

      float bestEntropy = 9999999999999; // inifinity
      int bestPredictor = 0;
      for(uint i = 0; i < _dataNodes[0]->getPredictors().size(); i++)
      {
         if(!_dataset->isSortedBy(i))
         {
            float entropy = getEntropyGain(i);
            if(entropy < bestEntropy)
            {
               bestEntropy = entropy;
               bestPredictor = i;
            }
         }
      }

      debug_print("DTreeNode::getNextPredictor: best predictor is %d\n", bestPredictor);
      return bestPredictor;
   }

   float entropy(vector<int> & counts)
   {
      float entropy = 0.0;
      float total = 0.0;
      for(uint i = 0; i < counts.size(); i++)
      {
         total += counts[i];
      }
      for(uint i = 0; i < counts.size(); i++)
      {
         if(counts[i] == 0)
         {
            continue;
         }
         float probability = (float) counts[i] / total;
         entropy -= probability * log2(probability);
      }
      return entropy;
   }

   float DTreeNode::getEntropyGain(int predictorIndex)
   {
      float gain = 0.0;
      float baseEntropy = 0.0;
      uint valueCount = _dataset->getUniqueValueCount(predictorIndex); // number of values the predictor can have
      uint resultCount = _dataset->getUniqueResultCount(); // number of possible results
      vector<int> values;
      vector<int> results;
      vector<float> probabilities;
      vector<vector<int>> counts; // counts[predictorValue][result]
      int totalCount = _dataNodes.size();

      for(uint i = 0; i < valueCount; i++)
      {
         counts.push_back(vector<int>());
         for(uint j = 0; j < resultCount; j++)
         {
            counts[i].push_back(0);
         }
      }


      // get the predictor values and results from each node
      for(uint i = 0; i < _dataNodes.size(); i++)
      {
         int value = _dataNodes[i]->getPredictor(predictorIndex);
         int result = _dataNodes[i]->getRawResult();
         values.push_back(value);
         results.push_back(result);
         counts[value][result]++;
      }

      /*
      //////////////////////////////////////////////////////
      // FOR DEBUGGING
      _dataset->printRaw();
      //print counts
      for(uint i = 0; i < valueCount; i++)
      {
         for(uint j = 0; j < resultCount; j++)
         {
            debug_print("DTreeNode::getEntropyGain: counts[%d][%d] is %d\n", i, j, counts[i][j]);
         }
      }
      //////////////////////////////////////////////////////*/

      // calculate base entropy
      for(uint i = 0; i < valueCount; i++)
      {
         int total = 0;
         for(uint j = 0; j < resultCount; j++)
         {
            total += counts[i][j];
         }
         baseEntropy += entropy(counts[i]) * total / totalCount;
      }

      debug_print("DTreeNode::getEntropyGain: baseEntropy for index %d is %f\n", predictorIndex, baseEntropy);


      return baseEntropy;

   }


} // namespace std
