#ifndef STD_DATANODE_HPP
#define STD_DATANODE_HPP

#include <vector>

namespace std {

	class DataNode {
	public:
		DataNode(vector<int> & row);

		int getPredictor(int index);
		vector<int> getPredictors();
		int getRawResult() { return _data.back(); }
		void print();

	private:
		vector<int> _data;
	};

} // namespace std

#endif  // STD_DATANODE_HPP
