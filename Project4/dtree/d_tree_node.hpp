#ifndef STD_DTREENODE_HPP
#define STD_DTREENODE_HPP

#include <vector>

#include "dataset.hpp"

namespace std {

	class DTreeNode {
	public:
		DTreeNode(Dataset * dataset); // constructor trains on initialization

		bool isResult() { return _isResult; } // returns true if this node is a result node
		int getRawResult() { return _result; } // returns result of this node
		DTreeNode * getNextNode(DataNode * dataNode); // returns next node based on dataNode

	private:
		Dataset * _dataset;
		vector<DataNode *> _dataNodes;
		int _predictorIndex;
		vector<DTreeNode *> _results;

		bool _isResult;
		int _result;

		int getNextPredictor();
		float getEntropyGain(int predictorIndex);
	};

} // namespace std

#endif  // STD_DTREENODE_HPP
