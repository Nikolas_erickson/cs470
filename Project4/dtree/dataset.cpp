#include "dataset.hpp"


#define DEBUG 0
#define debug_print(...) if (DEBUG) fprintf(stderr, __VA_ARGS__)

namespace std {

   Dataset::Dataset()
   {
      _size = 0;
   }

   Dataset::Dataset(vector<vector<string> *> * data)
   {
      // set size
      _size = data->size();

      // initialize unique values and sortedBy for each column of the data
      for(uint i = 0; i < data->at(0)->size(); i++)
      {
         _uniqueValues.push_back(new vector<string>);
         _sortedBy.push_back(false);
      }
      // remove the sortedby value for the result
      _sortedBy.erase(_sortedBy.begin());

      for(uint i = 0; i < _size; i++)
      {
         vector<string> * row = data->at(i); // raw data before conversion to integers
         vector<int> rowInt; // converted values
         for(uint j = 0; j < row->size(); j++)
         {
            int index = getUniqueValueIndex(row->at(j), j);
            rowInt.push_back(index);
         }
         DataNode * node = new DataNode(rowInt);
         _dataNodes.push_back(node);
      }
   }

   // get mapping of unique value to index in uniqueValues
   int Dataset::getUniqueValueIndex(string value, uint column)
   {
      for(uint i = 0; i < _uniqueValues[column]->size(); i++)
      {
         if(_uniqueValues[column]->at(i) == value)
         {
            return i;
         }
      }
      _uniqueValues[column]->push_back(value);
      return _uniqueValues[column]->size() - 1;
   }

   // get unique value from uniqueValues
   string Dataset::getUniqueValue(int index, uint column)
   {
      return _uniqueValues[column]->at(index);
   }

   // split dataset by unique values in column
   vector<Dataset *> Dataset::split(uint column)
   {
      // create empty datasets for each unique value
      vector<Dataset *> datasets;
      for(uint i = 0; i < _uniqueValues[column]->size(); i++)
      {
         datasets.push_back(new Dataset());
      }

      // copy dataNodes to appropriate datasets
      for(uint i = 0; i < _dataNodes.size(); i++)
      {
         datasets[_dataNodes[i]->getPredictors()[column]]->_dataNodes.push_back(_dataNodes[i]);
      }

      // copy over unique values
      for(uint i = 0; i < datasets.size(); i++)
      {
         for(uint j = 0; j < _uniqueValues.size(); j++)
         {
            datasets[i]->_uniqueValues.push_back(new vector<string>);
            for(uint k = 0; k < _uniqueValues[j]->size(); k++)
            {
               datasets[i]->_uniqueValues[j]->push_back(_uniqueValues[j]->at(k));
            }
         }
      }

      // copy over sortedBy
      for(uint i = 0; i < datasets.size(); i++)
      {
         for(uint j = 0; j < _sortedBy.size(); j++)
         {
            datasets[i]->_sortedBy.push_back(_sortedBy[j]);
         }
         datasets[i]->_sortedBy[column] = true;
      }

      // set size of each dataset
      for(uint i = 0; i < datasets.size(); i++)
      {
         datasets[i]->_size = datasets[i]->_dataNodes.size();
      }

      return datasets;
   }

   // select random sample of data
   Dataset Dataset::randomSelection(uint size)
   {
      debug_print("Dataset::randomSelection: size is %d\n", size);
      if(size > _size)
      {
         printf("Error: random selection size is larger than dataset size\n");
         return Dataset();
      }
      //printf("Random selection of %d\n", size);

      //create empty dataset
      Dataset other = Dataset();

      // set size of new dataset
      other._size = size;

      //copy over random dataNodes
      vector<int> indices;
      for(uint i = 0; i < _size; i++)
      {
         indices.push_back(i);
         //printf("index %d added \n", i);
      }
      for(uint i = 0; i < size; i++)
      {
         int index = rand() % indices.size();
         //printf("index %d selected \n", index);
         other._dataNodes.push_back(_dataNodes[indices[index]]);
         indices.erase(indices.begin() + index);
      }

      //copy over unique values
      for(uint i = 0; i < _uniqueValues.size(); i++)
      {
         other._uniqueValues.push_back(new vector<string>);
         for(uint j = 0; j < _uniqueValues[i]->size(); j++)
         {
            other._uniqueValues[i]->push_back(_uniqueValues[i]->at(j));
         }
      }

      //copy over sortedBy
      debug_print("Dataset::randomSelection: copying sortedBy\n");
      for(uint i = 0; i < _sortedBy.size(); i++)
      {
         other._sortedBy.push_back(_sortedBy[i]);
         debug_print("Dataset::randomSelection: sortedBy[%d] is %d\n", i, (int) _sortedBy[i]);
      }

      debug_print("Dataset::randomSelection: returning dataset\n");

      return other;
   }

   // select random sample of data
   Dataset Dataset::slice(uint start, uint size)
   {
      if(size + start > _size)
      {
         printf("Error: random selection size + start is larger than dataset size\n");
         return Dataset();
      }
      //printf("Random selection of %d\n", size);

      //create empty dataset
      Dataset other = Dataset();

      // set size of new dataset
      other._size = size;

      //copy over dataNodes
      for(uint i = 0; i < size; i++)
      {
         other._dataNodes.push_back(_dataNodes[start + i]);
      }

      //copy over unique values
      for(uint i = 0; i < _uniqueValues.size(); i++)
      {
         other._uniqueValues.push_back(new vector<string>);
         for(uint j = 0; j < _uniqueValues[i]->size(); j++)
         {
            other._uniqueValues[i]->push_back(_uniqueValues[i]->at(j));
         }
      }

      //copy over sortedBy
      debug_print("Dataset::slice: copying sortedBy\n");
      for(uint i = 0; i < _sortedBy.size(); i++)
      {
         other._sortedBy.push_back(_sortedBy[i]);
         debug_print("Dataset::slice: sortedBy[%d] is %d\n", i, (int) _sortedBy[i]);
      }

      debug_print("Dataset::slice: returning dataset\n");

      return other;
   }

   // returns true if all data has the same result
   bool Dataset::hasSameResult()
   {
      if(_size == 0)
      {
         return true;
      }
      int result = _dataNodes[0]->getRawResult();
      for(uint i = 1; i < _size; i++)
      {
         if(_dataNodes[i]->getRawResult() != result)
         {
            return false;
         }
      }
      return true;
   }

   // returns true if data is sorted by all columns
   bool Dataset::isFullySorted()
   {
      debug_print("Dataset::isFullySorted: _sortedBy.size() is %d\n", (int) _sortedBy.size());
      for(uint i = 0; i < _sortedBy.size(); i++)
      {
         if(!_sortedBy[i])
         {
            return false;
         }
      }
      return true;
   }

   // returns majority result
   int Dataset::getMajorityResult()
   {
      vector<int> counts;
      for(uint i = 0; i < _uniqueValues.back()->size(); i++)
      {
         counts.push_back(0);
      }
      for(uint i = 0; i < _size; i++)
      {
         counts[_dataNodes[i]->getRawResult()]++;
      }
      int maxIndex = 0;
      for(uint i = 1; i < counts.size(); i++)
      {
         if(counts[i] > counts[maxIndex])
         {
            maxIndex = i;
         }
      }
      return maxIndex;
   }

   // print raw representation of dataset, all values are mapped to integers
   void Dataset::printRaw()
   {
      for(uint i = 0; i < _size; i++)
      {
         _dataNodes[i]->print();
      }
   }

   // print dataset, all values are mapped to original input
   void Dataset::print()
   {
      for(uint i = 0; i < _size; i++)
      {
         vector<int> data = _dataNodes[i]->getPredictors();
         data.push_back(_dataNodes[i]->getRawResult());
         for(uint j = 0; j < data.size(); j++)
         {
            printf("%s ", getUniqueValue(data[j], j).c_str());
         }
         printf("\n");
      }
   }

} // namespace std
