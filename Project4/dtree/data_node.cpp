#include <stdio.h>

#include "data_node.hpp"

namespace std {

   DataNode::DataNode(vector<int> & row)
   {
      for(unsigned int i = 0; i < row.size(); i++)
      {
         _data.push_back(row[i]);
      }
   }

   int DataNode::getPredictor(int index)
   {
      if(index < 0 || index >= (int) _data.size() - 1)
      {
         fprintf(stderr, "DataNode::getPredictor: index out of bounds\n");
         return 0;
      }
      return _data[index];
   }

   vector<int> DataNode::getPredictors()
   {
      vector<int> predictors;
      for(unsigned int i = 0; i < _data.size() - 1; i++)
      {
         predictors.push_back(_data[i]);
      }
      return predictors;
   }

   void DataNode::print()
   {
      for(unsigned int i = 0; i < _data.size(); i++)
      {
         printf("%d ", _data[i]);
      }
      printf("\n");
   }

} // namespace std
