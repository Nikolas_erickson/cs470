#include "d_tree.hpp"

namespace std {

   // constructor trains on initialization
   DTree::DTree(Dataset & trainingData)
   {
      // create root node, which will create the rest of the tree
      _root = new DTreeNode(&trainingData);
   }

   // test decision tree against dataset (print results to console
   void DTree::test(Dataset testData, bool csvOutput)
   {
      int correct = 0;
      int total = testData.getSize();

      for(uint i = 0; i < testData.getSize(); i++)
      {
         DataNode * data = testData.getDataNode(i);
         DTreeNode * treeNode = _root;
         while(!treeNode->isResult())
         {
            treeNode = treeNode->getNextNode(data);
         }
         if(treeNode->isResult())
         {
            if(treeNode->getRawResult() == data->getRawResult())
            {
               correct++;
            }
         }
      }
      if(csvOutput)
      {
         printf("%d,%d\n", correct, total);
      }
      else
      {
         printf("Results: %d / %d\n", correct, total);
      }
   }

} // namespace std
