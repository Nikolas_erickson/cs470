#ifndef STD_DTREE_HPP
#define STD_DTREE_HPP

#include "dataset.hpp"
#include "d_tree_node.hpp"

namespace std {

	class DTree {
	public:
		DTree(Dataset & trainingData); // constructor trains on initialization

		void test(Dataset testData, bool csvOutput = false); // test decision tree against dataset (print results to console

	private:
		DTreeNode * _root;
	};

} // namespace std

#endif  // STD_DTREE_HPP
