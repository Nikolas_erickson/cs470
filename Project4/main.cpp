#include <time.h>

#include "libs/timer.h"
#include "libs/csv.h"
#include "dtree/dataset.hpp"
#include "dtree/d_tree.hpp"

using namespace std;

int main()
{
   // seed rand
   srand(time(NULL));

   // read data from CSV file
   CSVObject csv = CSVObject("data.CSV");
   csv.setDelimitWhitespace(true);
   csv.setSkipFirstColumn(true);
   csv.readData(-1, 1);
   //csv.printData();
   vector<vector<string> *> * data = csv.getData();

   // create dataset from parsed CSV
   Dataset dataset = Dataset(data);
   //dataset.print();
   // test different sizes of training data
   vector<int> testCases = { 2,5,10,20,50 };

   for(uint i = 0; i < testCases.size(); i++)
   {
      printf("\n\nTest case %d\n", testCases[i]);
      // for 10 iterations, do this and save the data for comparison
      for(uint j = 0; j < 20; j++)
      {
         // get training data of size testCases[i]
         Dataset sampleData = dataset.randomSelection(testCases[i]);

         // initialize and train decision tree
         DTree dtree = DTree(sampleData);

         // test decision tree against full dataset
         dtree.test(dataset);
      }
   }

   return 0;
}