# Graph Coloring CSP Solver

This repository contains a program that solves a decision tree problem. Given a CSV file with 100 cases of predictor data and the results, this program generates a decision tree to give a result given a set of predictors.

This is done for subsets of the given data, and the resulting tree is tested against the full set of the data to compare how well it predicts the outcome based on the amount of training data used.

## Usage

1. Clone the repository
2. Navigate to the project directory
3. Run the program with the input file

## Algorithm

This program uses an algorithm that does things.

## Output

The program will output the stuff.

## Contributing

Contributions to this project are welcome. If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.
