#ifndef TIMER_H
#define TIMER_H

#include <cstdint>

uint64_t millis();

#endif //TIMER_H