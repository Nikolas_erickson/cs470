#include <chrono>

#include "timer.h"

using namespace std;

// get time for debugging
uint64_t millis()
{
   uint64_t ms = chrono::duration_cast<chrono::milliseconds>(
      chrono::high_resolution_clock::now().time_since_epoch())
      .count();
   return ms;
}