#include <string.h>
#include <stdio.h>

#include "csv.h"

#define MAXLINELENGTH 1024

using namespace std;

// constructor only initializes variables, does not read the data
CSVObject::CSVObject(const char * filename)
{
   this->filename = strdup(filename);

   // set flags
   fileRead = false;
   delimitWhitespace = false;
   skipEmptyLinesOnRead = false;
   skipFirstColumn = false;
}

// sets flag to delimit based on whitespace
void CSVObject::setDelimitWhitespace(bool delimitWhitespace)
{
   this->delimitWhitespace = delimitWhitespace;

}

// sets flag to skip empty lines on read
void CSVObject::setSkipEmptyLinesOnRead(bool skipEmptyLinesOnRead)
{
   this->skipEmptyLinesOnRead = skipEmptyLinesOnRead;
}

// sets flag to skip first column on read
void CSVObject::setSkipFirstColumn(bool skipFirstColumn)
{
   this->skipFirstColumn = skipFirstColumn;
}

// reads data from file
// first it reads in raw data, then it tokenizes the data based on delimiters
void CSVObject::readData(int linesToRead, int linesToSkip)
{
   // get raw data from file
   int line = 1;
   int linesRead = 0;
   char buffer[MAXLINELENGTH];
   FILE * file = fopen(filename, "r");
   if(file == NULL)
   {
      fprintf(stderr, "ERROR: file not found\n");
      fileRead = false;
      return;
   }
   while(!feof(file))
   {
      if(fgets(buffer, MAXLINELENGTH, file) != NULL)
      {
         if(line <= linesToSkip)
         {
            line++;
            continue;
         }
         if(skipEmptyLinesOnRead && buffer[0] == '\n')
         {
            // skip empty lines
            continue;
         }
         if(linesToRead != -1 && linesRead >= linesToRead)
         {
            break;
         }
         rawData.push_back(strdup(buffer));
         linesRead++;

      }
   }
   fclose(file);


   // set file read flag
   fileRead = true;

   // tokenize data
   for(uint i = 0; i < rawData.size(); i++)
   {
      vector<string> * tokens = new vector<string>;
      char * delimeters = (char *) ",\n";
      if(delimitWhitespace)
      {
         delimeters = (char *) " \t,\n";
      }
      char * token = strtok(rawData[i], delimeters);
      while(token != NULL)
      {
         tokens->push_back(string(token));
         token = strtok(NULL, delimeters);
      }
      // remove trailing newline and carriage return on last token
      if(tokens->back().size() > 0)
      {
         if(tokens->back().at(tokens->back().size() - 1) == '\n')
         {
            tokens->back().erase(tokens->back().size() - 1);
         }
         if(tokens->back().at(tokens->back().size() - 1) == '\r')
         {
            tokens->back().erase(tokens->back().size() - 1);
         }
      }
      if(skipFirstColumn)
      {
         tokens->erase(tokens->begin());
      }
      data.push_back(tokens);
   }

   // free raw data
   for(uint i = 0; i < rawData.size(); i++)
   {
      free(rawData[i]);
   }
}

void CSVObject::printData()
{
   for(uint i = 0; i < data.size(); i++)
   {
      for(uint j = 0; j < data[i]->size(); j++)
      {
         printf("%s, ", data[i]->at(j).c_str());
      }
      printf("\n");
   }
}


vector<vector<string> *> * CSVObject::getData()
{
   if(!fileRead)
   {
      fprintf(stderr, "ERROR: file not read\n");
      return nullptr;
   }
   return &data;
}