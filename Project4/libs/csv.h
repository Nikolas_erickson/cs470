#ifndef CSV_H
#define CSV_H

#include <vector>
#include <string>

using namespace std;

struct CSVObject
{
private:
   char * filename;
   vector<char *> rawData;
   vector<vector<string> *> data;

   // flags
   bool fileRead; // true if file has been read
   bool delimitWhitespace; // true if whitespace is a delimiter
   bool skipEmptyLinesOnRead; // true if empty lines should be skipped
   bool skipFirstColumn; // true if first column should be skipped




public:
   CSVObject(const char * filename);

   void setDelimitWhitespace(bool delimitWhitespace);
   void setSkipEmptyLinesOnRead(bool skipEmptyLinesOnRead);
   void setSkipFirstColumn(bool skipFirstColumn);
   void readData(int linesToRead = -1, int linesToSkip = 0);

   void printData();

   vector<vector<string> *> * getData();
};

#endif //CSV_H