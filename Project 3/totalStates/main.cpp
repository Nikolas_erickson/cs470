#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "csp.h"

#define NUMLINES 31
#define MAXLINELENGTH 120

int main()
{
   // get raw data from file
   char * inputFile = (char *) "CSPData.csv";
   char * rawData[NUMLINES]; // Array to store the raw data from the file

   FILE * file = fopen(inputFile, "r");
   if(file == NULL)
   {
      printf("file not found\n");
      return 1;
   }
   while(!feof(file))
   {
      for(int i = 0; i < NUMLINES; i++)
      {
         rawData[i] = (char *) malloc(MAXLINELENGTH);
         fgets(rawData[i], MAXLINELENGTH, file);
         printf("%s\n", rawData[i]);
      }
   }

   // parse the raw data
   // length is 1 less than number of lines because the first line is empty
   int map[NUMLINES - 1][NUMLINES - 1]; // map of the CSP problem, 1 if there is a constraint between the two variables, 0 otherwise
   for(int i = 1; i < NUMLINES; i++)
   {
      char * token = strtok(rawData[i] + 3 + i, ",\n");
      printf("%s: \n", token);
      for(int j = i; j < NUMLINES - 1; j++)
      {
         map[i - 1][j] = atoi(token);
         token = strtok(NULL, ",\n");
         printf("%d ", map[i - 1][j]);
      }printf("\n");
   }

   // mirror map to other side of diagonal
   for(int i = 0; i < NUMLINES - 1; i++)
   {
      for(int j = 0; j < i; j++)
      {
         map[i][j] = map[j][i];
      }
   }

   // set diagonal to zero
   for(int i = 0; i < NUMLINES - 1; i++)
   {
      map[i][i] = 0;
   }

   // run CSP algorithm, color map

   long long  numStates = colorMap(map, NUMLINES - 1, 4);
   printf("Number of states: %lld\n", numStates);

   // output results

   // free memory
   for(int i = 0; i < NUMLINES; i++)
   {
      free(rawData[i]);
   }

   return 0;
}