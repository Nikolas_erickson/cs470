#include <iostream>
#include <queue>

#include "map.h"
#include "node.h"


using namespace std;



Map::Map(int mapSize, int numColors)
{
   this->mapSize = mapSize;
   this->numColors = numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i].setDomain(numColors);
      nodes[i].setId(i);
   }

}

Map::Map(Map & other)
{
   this->mapSize = other.mapSize;
   this->numColors = other.numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i] = other.nodes[i];
   }

}
Map::~Map()
{
   delete[] nodes;
}

bool Map::complete()
{
   for(int i = 0; i < mapSize; i++)
   {
      if(!nodes[i].colored)
      {
         return false;
      }
   }
   return true;
}

bool Map::conflicted()
{
   for(int i = 0; i < mapSize; i++)
   {
      if(nodes[i].domain.size() == 0 && !nodes[i].colored)
      {
         return true;
      }
   }
   return false;
}

// forward propagation - input should be a node that has just been colored
// returns false if the forward propagation causes a conflict
bool Map::propagate(int nodeIndex)
{
   Node * node = &nodes[nodeIndex];
   if(!node->colored)
   {
      printf("ERROR: trying to propagate a node that isn't colored\n");
      printf("nodeIndex: %d\n", nodeIndex);
      return false;
   }
   std::queue<Node *> q;
   for(int i = 0; i < node->numNeighbors; i++)
   {
      q.push(&nodes[node->neighbors[i]]);
   }
   while(!q.empty())
   {
      Node * n = q.front();
      q.pop();
      n->removeColor(node->color);
      if(!n->colored && n->domain.size() == 0)
      {
         return false;
      }
      if(n->domain.size() == 1 && !n->colored)
      {
         n->setColor(n->domain[0]);
         bool subResult = propagate(n->id);
         if(!subResult)
         {
            return false;
         }
      }
      if(n->colored && n->color == node->color)
      {
         return false;
      }
   }
   return true;
}

void Map::operator=(const Map & other)
{
   this->mapSize = other.mapSize;
   this->numColors = other.numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i] = other.nodes[i];
   }
}

void Map::print()
{
   for(int i = 0; i < mapSize; i++)
   {
      printf("Node %d: ", i);
      if(nodes[i].colored)
      {
         printf("color: %d\n", nodes[i].color);
      }
      else
      {
         printf("uncolored\n");
      }
      printf("\t\tNeighbors: ");
      for(int j = 0; j < (int) nodes[i].numNeighbors; j++)
      {
         printf("%d ", nodes[i].neighbors[j]);
      }
      printf("\n");
   }
}