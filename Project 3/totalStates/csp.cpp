#include <iostream>
#include <vector>

#include "csp.h"
#include "map.h"
#include "node.h"

using namespace std;

#define MAPSIZE 30

bool fail = false;

Node * getNextNode(Map & mapObj)
{
   // first by fewest remaining values
   vector<Node *> q;
   int minDomain = MAPSIZE + 1;
   for(int i = 0; i < mapObj.mapSize; i++)
   {
      if(!mapObj.nodes[i].colored)
      {
         if((int) mapObj.nodes[i].domain.size() < minDomain)
         {
            q = vector<Node *>();
            q.push_back(&mapObj.nodes[i]);
            minDomain = mapObj.nodes[i].domain.size();
         }
         else if((int) mapObj.nodes[i].domain.size() == minDomain)
         {
            q.push_back(&mapObj.nodes[i]);
         }
      }
   }

   if(q.size() == 0) // shouldn't be possible
   {
      printf("\aERROR: no free nodes, but also not an end state\n\n");
      return nullptr;
   }

   // then by most constraints
   int mostNeighbors = -1;
   Node * nextNode = q[0];
   for(int i = 0; i < (int) q.size(); i++)
   {
      if(q[i]->numNeighbors > mostNeighbors)
      {
         nextNode = q[i];
         mostNeighbors = q[i]->numNeighbors;
      }
   }
   return nextNode;
}

long long colorMapRec(Map & mapObj, int depth, long long & checked)
{
   long long numStates = 0;
   // check for an end state
   if(mapObj.complete())
   {
      checked++;
      if(checked % 10000 == 0) printf("checked %lld states, solved %lld states\r", checked, numStates);
      return 1;
   }
   if(mapObj.conflicted())
   {
      checked++;
      if(checked % 10000 == 0) printf("checked %lld states, solved %lld states\r", checked, numStates);
      return 0;
   }

   // if not an end state
   // find the next node
   Node * nextNode = getNextNode(mapObj);
   if(nextNode == nullptr)
   {
      printf("ERROR: no next node\n");
      return 0;
   }

   // try each color in the domain
   for(int i = 0; i < (int) nextNode->domain.size(); i++)
   {
      //printf("Trying node %d at depth %d: \n", nextNode->id, depth);
      Map mapCopy(mapObj);
      mapCopy.nodes[nextNode->id].setColor(nextNode->domain[i]);
      //printf("with color: %d\n", nextNode->domain[i]);
      if(mapCopy.propagate(nextNode->id))
      {
         //mapCopy.print();
         numStates += colorMapRec(mapCopy, depth + 1, checked);
         if(depth == 1) printf("checked %lld states, solved %lld states\n", checked, numStates);
      }

      // if depth is 1, then we are at the root of the tree
      // solution is numStates * numColors
      if(depth == 1)
      {
         return numStates * mapObj.numColors;
      }
   }
   return numStates;
}

long long colorMap(int map[30][30], int mapSize, int numColors)
{
   Map mapObj(mapSize, numColors);

   for(int i = 0; i < mapSize; i++)
   {
      mapObj.nodes[i].id = i;
      for(int j = 0; j < mapSize; j++)
      {
         if(map[i][j] == 1)
         {
            mapObj.nodes[i].neighbors.push_back(j);
            mapObj.nodes[i].numNeighbors++;
         }
      }
   }
   long long checked = 0;

   printf("enumerating states that satisfy constraints\n");
   mapObj.print();
   int numStates = colorMapRec(mapObj, 1, checked);

   return numStates;
}