this is an attempt to check how many solutions there are to this CSP.
The results are inconclusive because the search space is too large.
I might be able to improve the algorithm used for searching the space, but the total space has 4^30 possible states.
Many of the branches of the search tree can be pruned, but my search algorithm is still too inefficient to search for the total number of solved states.
