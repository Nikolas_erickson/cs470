#include <iostream>
#include <vector>

#include "csp.h"
#include "map.h"
#include "node.h"

using namespace std;

#define MAPSIZE 30

bool fail = false;

Node * getNextNode(Map & mapObj)
{
   // first by fewest remaining values
   vector<Node *> q;
   int minDomain = MAPSIZE + 1;
   for(int i = 0; i < mapObj.mapSize; i++)
   {
      if(!mapObj.nodes[i].colored)
      {
         if((int) mapObj.nodes[i].domain.size() < minDomain)
         {
            q = vector<Node *>();
            q.push_back(&mapObj.nodes[i]);
            minDomain = mapObj.nodes[i].domain.size();
         }
         else if((int) mapObj.nodes[i].domain.size() == minDomain)
         {
            q.push_back(&mapObj.nodes[i]);
         }
      }
   }

   if(q.size() == 0) // shouldn't be possible
   {
      printf("\aERROR: no free nodes, but also not an end state\n\n");
      return nullptr;
   }

   // then by most constraints
   int mostNeighbors = -1;
   Node * nextNode = q[0];
   for(int i = 0; i < (int) q.size(); i++)
   {
      if(q[i]->numNeighbors > mostNeighbors)
      {
         nextNode = q[i];
         mostNeighbors = q[i]->numNeighbors;
      }
   }
   return nextNode;
}

bool colorMapRec(Map & mapObj, int depth = 0)
{
   // check for an end state
   if(mapObj.complete())
   {
      return true;
   }
   if(mapObj.conflicted())
   {
      return false;
   }

   // if not an end state
   // find the next node
   Node * nextNode = getNextNode(mapObj);
   if(nextNode == nullptr)
   {
      return true;
   }

   // try each color in the domain
   for(int i = 0; i < (int) nextNode->domain.size(); i++)
   {
      printf("Trying node %d at depth %d: \n", nextNode->id, depth);
      Map mapCopy(mapObj);
      mapCopy.nodes[nextNode->id].setColor(nextNode->domain[i]);
      printf("with color: %d\n", nextNode->domain[i]);
      if(mapCopy.propagate(nextNode->id))
      {
         printf("propagated! current state: \n");
         mapCopy.print();
         if(colorMapRec(mapCopy, depth + 1))
         {
            mapObj = mapCopy;
            //print color
            printf("node %d, color %d\n", nextNode->id, nextNode->color);
            return true;
         }
      }
      else
      {
         printf("Failed state: \n");
         mapCopy.print();
      }
      if(depth == 1)
      {
         return false;
      }
   }
   return false;
}

bool colorMap(int map[30][30], int mapSize, int numColors)
{
   Map mapObj(mapSize, numColors);

   for(int i = 0; i < mapSize; i++)
   {
      for(int j = 0; j < mapSize; j++)
      {
         if(map[i][j] == 1)
         {
            mapObj.nodes[i].neighbors.push_back(j);
            mapObj.nodes[i].numNeighbors++;
         }
      }
   }
   printf("Initial State:\n");
   mapObj.print();
   printf("\n\n");

   if(colorMapRec(mapObj, 1))
   {
      mapObj.print();
      return true;
   }
   else
   {
      printf("No solution found with %d colors\n", numColors);
      return false;
   }
}