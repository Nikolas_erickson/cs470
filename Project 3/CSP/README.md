# Graph Coloring CSP Solver

This repository contains a program that solves the graph coloring problem using a constraint satisfaction approach with backtracking. Given an input graph, it finds the minimum number of colors required to color the nodes such that no adjacent nodes have the same color.

## Usage

1. Clone the repository
2. Navigate to the project directory
3. Run the program with the input graph file

## Algorithm

The program uses a backtracking algorithm with constraint propagation to solve the graph coloring problem. It explores the search space by assigning colors to nodes and backtracking when a constraint is violated. The algorithm employs the following heuristics to guide the search:

- Nodes with smaller remaining domains (fewer available colors) are prioritized.
- Within nodes with equally small domains, nodes with more neighbors are prioritized.
- If multiple nodes satisfy the above criteria, the first node found is selected.

The algorithm also propagates the constraints by removing assigned colors from the domains of neighboring nodes, further reducing the search space.

## Output

The program will output the minimum number of colors required to color the graph, along with the time taken to solve the problem.

## Contributing

Contributions to this project are welcome. If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.
