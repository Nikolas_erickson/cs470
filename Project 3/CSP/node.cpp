#include <iostream>
#include <queue>

#include "map.h"
#include "node.h"

using namespace std;

/*
struct Node
{
   int id;
   int color;
   bool colored;
   int numColors;
   int numNeighbors;
   std::vector<int> domain;
   std::vector<int> neighbors;
}*/

Node::Node()
{
   id = 0;
   color = 0;
   colored = false;
   numColors = 0;
   numNeighbors = 0;
}
void Node::operator=(const Node & other)
{
   id = other.id;
   color = other.color;
   colored = other.colored;
   numColors = other.numColors;
   numNeighbors = other.numNeighbors;
   for(int i = 0; i < (int) other.domain.size(); i++)
   {
      domain.push_back(other.domain[i]);
   }
   for(int i = 0; i < (int) other.neighbors.size(); i++)
   {
      neighbors.push_back(other.neighbors[i]);
   }
}

Node::~Node()
{
   domain.clear();
   neighbors.clear();
}

void Node::setId(int newId)
{
   id = newId;
}

// initializes the domain to contain the numbers 1 through numColors
void Node::setDomain(int numColors)
{
   printf("setting node %d domain to %d\n", id, numColors);
   this->numColors = numColors;
   domain.clear();
   for(int i = 1; i <= numColors; i++)
   {
      domain.push_back(i);
   }
}

// assumes the neighbor has not already been added
// does not prevent duplicates
void Node::addNeighbor(int neighbor)
{
   neighbors.push_back(neighbor);
   numNeighbors++;
}

// sets the color of node and colored flag to true
void Node::setColor(int color)
{
   printf("setting node %d to color %d\n", id, color);
   this->color = color;
   this->domain.clear();
   this->domain.push_back(color);
   colored = true;
}

// returns true if the color was removed from the domain
// or if the node was already colored a different color
bool Node::removeColor(int color)
{
   if(colored && this->color != color)
   {
      return true;
   }
   for(int i = 0; i < (int) domain.size(); i++)
   {
      if(domain[i] == color)
      {
         domain.erase(domain.begin() + i);
         return true;
      }
   }
   return false;
}