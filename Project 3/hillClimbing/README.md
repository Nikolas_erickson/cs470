# Graph Coloring CSP Solver

This repository contains a program that solves the graph coloring problem using a hill climbing algorithm. Given an input graph, it finds a valid coloring of the nodes such that no adjacent nodes have the same color.

## Usage

1. Clone the repository
2. Navigate to the project directory
3. Run the program with the input graph file

## Algorithm

The program uses a hill climbing algorithm to solve the graph coloring problem. It starts with a random coloring of the nodes and iteratively tries to improve the solution by identifying nodes with conflicts (i.e., adjacent nodes with the same color) and reassigning their colors to reduce the number of conflicts. This process continues until a local optimum is reached, where no further improvements can be made.

If the local optimum is not a valid coloring (i.e., it still has conflicts), the algorithm restarts with a new random coloring and repeats the process until a valid solution is found.

## Output

The program will output the minimum number of colors required to color the graph, along with the time taken to solve the problem.

## Contributing

Contributions to this project are welcome. If you find any issues or have suggestions for improvement, please open an issue or submit a pull request.
