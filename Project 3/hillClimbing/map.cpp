#include <iostream>
#include <queue>

#include "map.h"
#include "node.h"


using namespace std;



Map::Map(int mapSize, int numColors)
{
   this->mapSize = mapSize;
   this->numColors = numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i].setDomain(numColors);
      nodes[i].setId(i);
   }

}

Map::Map(Map & other)
{
   this->mapSize = other.mapSize;
   this->numColors = other.numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i] = other.nodes[i];
   }

}
Map::~Map()
{
   delete[] nodes;
}

bool Map::complete()
{
   for(int i = 0; i < mapSize; i++)
   {
      if(!nodes[i].colored)
      {
         return false;
      }
   }
   return true;
}

bool Map::conflicted()
{
   for(int i = 0; i < mapSize; i++)
   {
      if(nodes[i].domain.size() == 0 && !nodes[i].colored)
      {
         return true;
      }
   }
   return false;
}

int Map::totalConflicts()
{
   int total = 0;
   for(int i = 0; i < mapSize; i++)
   {
      for(int j = 0; j < nodes[i].numNeighbors; j++)
      {
         if(nodes[i].colored && nodes[nodes[i].neighbors[j]].colored && nodes[i].color == nodes[nodes[i].neighbors[j]].color)
         {
            total++;
         }
      }
   }
   return total;
}


Node * Map::getMostConflictedNode()
{
   int mostConflicts = -1;
   Node * mostConflictedNode = nullptr;
   for(int i = 0; i < mapSize; i++)
   {
      int conflicts = 0;
      for(int j = 0; j < nodes[i].numNeighbors; j++)
      {
         if(nodes[i].colored && nodes[nodes[i].neighbors[j]].colored && nodes[i].color == nodes[nodes[i].neighbors[j]].color)
         {
            conflicts++;
         }
      }
      if(conflicts > mostConflicts)
      {
         mostConflicts = conflicts;
         mostConflictedNode = &nodes[i];
      }
   }
   return mostConflictedNode;
}

// forward propagation - input should be a node that has just been colored
// returns false if the forward propagation causes a conflict
bool Map::propagate(int nodeIndex)
{
   Node * node = &nodes[nodeIndex];
   if(!node->colored)
   {
      printf("ERROR: trying to propagate a node that isn't colored\n");
      printf("nodeIndex: %d\n", nodeIndex);
      return false;
   }
   std::queue<Node *> q;
   for(int i = 0; i < node->numNeighbors; i++)
   {
      q.push(&nodes[node->neighbors[i]]);
      printf("PUSH neighbor %d\n", node->neighbors[i]);
   }
   while(!q.empty())
   {
      Node * n = q.front();
      q.pop();
      printf("POP  node %d\n", n->id);
      n->removeColor(node->color);
      printf("removed color %d\n", node->color);
      if(!n->colored && n->domain.size() == 0)
      {
         printf("domain size is 0\n");
         return false;
      }
      if(n->domain.size() == 1 && !n->colored)
      {
         printf("domain size is 1\n");
         n->setColor(n->domain[0]);
         for(int i = 0; i < n->numNeighbors; i++)
         {
            q.push(&nodes[n->neighbors[i]]);
            printf("PUSH neighbor %d\n", n->neighbors[i]);
         }
      }
      if(n->colored && n->color == node->color)
      {
         return false;
      }
   }
   return true;
}

void Map::operator=(const Map & other)
{
   this->mapSize = other.mapSize;
   this->numColors = other.numColors;
   this->nodes = new Node[mapSize];
   for(int i = 0; i < mapSize; i++)
   {
      nodes[i] = other.nodes[i];
   }
}

void Map::print()
{
   for(int i = 0; i < mapSize; i++)
   {
      printf("Node %d: %d", i, nodes[i].color);
      printf("\t\tNeighbors: ");
      for(int j = 0; j < (int) nodes[i].numNeighbors; j++)
      {
         printf("%d ", nodes[i].neighbors[j]);
      }
      printf("\n");
   }
}