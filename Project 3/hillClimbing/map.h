#ifndef MAP_H
#define MAP_H

#include "node.h"

struct Map
{
   int mapSize;
   int numColors;
   Node * nodes;

   Map(int mapSize, int numColors);
   Map(Map & other); // copy constructor
   ~Map();

   bool complete();
   bool conflicted();

   int totalConflicts();
   Node * getMostConflictedNode();

   bool propagate(int nodeIndex);

   void operator=(const Map & other);

   void print();
};

#endif // MAP_H