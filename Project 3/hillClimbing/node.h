#ifndef NODE_H
#define NODE_H

#include <vector>

struct Node
{
   int id;
   int color;
   bool colored;
   int numColors;
   int numNeighbors;
   std::vector<int> domain;
   std::vector<int> neighbors;

   Node();
   void operator=(const Node & other);
   ~Node();


   void setId(int newId);

   // initializes the domain to contain the numbers 1 through numColors
   void setDomain(int numColors);

   // assumes the neighbor has not already been added
   // does not prevent duplicates
   void addNeighbor(int neighbor);

   // sets the color of node and colored flag to true
   void setColor(int color);

   // returns true if the color was removed from the domain
   // or if the node was already colored a different color
   bool removeColor(int color);
};

#endif // NODE_H