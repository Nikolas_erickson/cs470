#include <iostream>
#include <vector>

#include "hillClimbing.h"
#include "map.h"
#include "node.h"

using namespace std;

#define MAPSIZE 30


bool findLocalOptimum(Map mapObj)
{
   int numConflicts = mapObj.totalConflicts();
   int newConflicts = 9999999;
   bool improved = false;
   Map * newMap;
   int iterations = 0;

   printf("Finding local optimum\n");
   printf("Initial number of conflicts: %d\n", numConflicts);
   do
   {
      improved = false;
      // make copy of map
      newMap = new Map(mapObj);
      // get most conflicted node
      Node * mostConflicted = newMap->getMostConflictedNode();
      // check all states of that node for best state
      for(int i = 0; i < (int) mostConflicted->domain.size(); i++)
      {
         mostConflicted->color = mostConflicted->domain[i];
         newConflicts = newMap->totalConflicts();
         if(newConflicts < numConflicts)
         {
            numConflicts = newConflicts;
            mapObj = *newMap;
            improved = true;
         }
      }
      iterations++;
      printf("Iteration %d\n", iterations);
      printf("New number of conflicts: %d\n", numConflicts);
   }
   while(improved);
   printf("Local Optimum Found:\n");
   printf("Number of conflicts: %d\n", numConflicts);
   mapObj.print();
   return numConflicts == 0;
}

bool colorMap(int map[30][30], int mapSize, int numColors)
{
   // seed rand
   srand(time(NULL));

   printf("Initializing map with random values\n");

   Map mapObj(mapSize, numColors);

   for(int i = 0; i < mapSize; i++)
   {
      mapObj.nodes[i].setId(i);
      mapObj.nodes[i].setColor(rand() % numColors + 1);
      for(int j = 0; j < mapSize; j++)
      {
         if(map[i][j] == 1)
         {
            mapObj.nodes[i].neighbors.push_back(j);
            mapObj.nodes[i].numNeighbors++;
         }
      }
   }
   printf("Initial State:\n");
   mapObj.print();
   printf("\n\n");

   return findLocalOptimum(mapObj);
}