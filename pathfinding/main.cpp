#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "grid.h"
#include "state.h"

#include "pfBFS.h"
#include "lowestCost.h"
#include "greedyBestFirst.h"
#include "aStarManhattan.h"
#include "aStarEuclidean.h"
#include "aStarXcoord.h"


using namespace std;



int main(int argc, char * argv[])
{
   // get input filename, defaults to "grid.txt"
   string filename = "grid.txt";
   if(argc == 2)
   {
      filename = argv[1];
   }

   // load grid from file
   Grid field(filename.c_str());

   cout << "Starting grid:" << endl;
   field.print();

   // run pathfinding BFS
   cout << endl << "Running BFS..." << endl << endl;
   Grid bfsField(field);
   State * finalState = bfs(bfsField);
   bfsField.setPath(finalState);
   bfsField.print();
   bfsField.printStats();

   // run pathfinding lowest cost
   cout << endl << "Running lowest cost search..." << endl << endl;
   Grid lowestCostField(field);
   finalState = lowestCostSearch(lowestCostField);
   lowestCostField.setPath(finalState);
   lowestCostField.print();
   lowestCostField.printStats();

   // run pathfinding greedy best first
   cout << endl << "Running greedy best first search..." << endl << endl;
   Grid greedyField(field);
   finalState = greedyBestSearch(greedyField);
   greedyField.setPath(finalState);
   greedyField.print();
   greedyField.printStats();

   // run pathfinding A Star Manhattan
   cout << endl << "Running a* manhattan search..." << endl << endl;
   Grid astarm(field);
   finalState = aStarManhattanSearch(astarm);
   astarm.setPath(finalState);
   astarm.print();
   astarm.printStats();

   // run pathfinding A Star Euclidean
   cout << endl << "Running a* euclidean search..." << endl << endl;
   Grid astare(field);
   finalState = aStarEuclideanSearch(astare);
   astare.setPath(finalState);
   astare.print();
   astare.printStats();

   // run pathfinding A Star based on x coordinate only
   cout << endl << "Running a* search with the heuristic equal to abs(node.x-end.x)..." << endl << endl;
   Grid astarx(field);
   finalState = aStarXcoordSearch(astarx);
   astarx.setPath(finalState);
   astarx.print();
   astarx.printStats();


   return 0;
}