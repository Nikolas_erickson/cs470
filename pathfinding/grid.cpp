#include <iostream>
#include <fstream>
#include <vector>
#include <string>



#define OPEN '+'
#define CLOSED '-'
#define PATH 'P'
#define WALL 'X'
#define START 'S'
#define END 'E'

#include "grid.h"
#include "state.h"


using namespace std;

Grid::Grid()
{
   m_width = 0;
   m_height = 0;
   m_start = { 0, 0, 'S' };
   m_end = { 0, 0, 'E' };
   m_grid = nullptr;
}

Grid::Grid(const char * filename) : Grid()
{
   load(filename);
}

Grid::Grid(Grid & other)
{
   m_width = other.m_width;
   m_height = other.m_height;
   m_start = other.m_start;
   m_end = other.m_end;
   m_grid = new char * [m_width];
   for(int i = 0; i < m_width; i++)
   {
      m_grid[i] = new char[m_height];
      for(int j = 0; j < m_height; j++)
      {
         m_grid[i][j] = other.m_grid[i][j];
      }
   }
}

Grid::~Grid()
{
   if(m_grid)
   {
      for(int i = 0; i < m_width; i++)
      {
         delete[] m_grid[i];
      }
      delete[] m_grid;
   }
}

void Grid::load(const char * filename)
{
   // clear old grid if it exists
   if(m_grid)
   {
      for(int i = 0; i < m_width; i++)
      {
         delete[] m_grid[i];
      }
      delete[] m_grid;
   }

   // read file
   ifstream inputFile(filename);
   if(!inputFile.is_open())
   {
      cout << "Error opening file: " << filename << endl;
      return;
   }
   vector<string> grid;
   while(!inputFile.eof())
   {
      string line;
      getline(inputFile, line);
      grid.push_back(line);
   }
   inputFile.close();

   // parse input
   m_height = grid.size();
   m_width = grid[0].size();
   m_grid = new char * [m_width];
   for(int i = 0; i < m_width; i++)
   {
      m_grid[i] = new char[m_height];
      for(int j = 0; j < m_height; j++)
      {
         m_grid[i][j] = grid[j][i];
         if(m_grid[i][j] == 'S')
         {
            m_start = { i, j, 'S' };
         }
         else if(m_grid[i][j] == 'E')
         {
            m_end = { i, j, 'E' };
         }
      }
   }

}

void Grid::print()
{
   // print grid and related data
   for(int i = 0; i < m_height; i++)
   {
      for(int j = 0; j < m_width; j++)
      {
         switch(m_grid[j][i])
         {
         case 'X': // wall
            cout << "\u001b[44m";
            break;
         case 'S': // start
            cout << "\033[92m";
            break;
         case 'E': // end
            cout << "\033[91m";
            break;
         case 'P': // path
            cout << "\033[95m";
            break;
         case '+': // open
            cout << "\033[93m";
            break;
         case '-': // closed
            cout << "\033[96m";
            break;
         }
         cout << m_grid[j][i];
         cout << "\033[0m";
      }
      cout << endl;
   }
   cout << "Start: " << m_start.x << ", " << m_start.y << endl;
   cout << "End: " << m_end.x << ", " << m_end.y << endl;
   cout << "Width: " << m_width << endl;
   cout << "Height: " << m_height << endl << endl;
}

void Grid::printStats()
{
   // print stats
   int stats[5] = { 0 }; // stats stored in array, 0 = open, 1 = closed, 2 = path, 3 = walls, 4 = total spaces
   for(int i = 0; i < m_height; i++)
   {
      for(int j = 0; j < m_width; j++)
      {
         stats[4]++;
         switch(m_grid[j][i])
         {
         case '+':
            stats[0]++;
            break;
         case '-':
            stats[1]++;
            break;
         case 'S':
         case 'E':
         case 'P':
            stats[2]++;
            stats[1]++;
            break;
         case 'X':
            stats[3]++;
            break;
         }
         stats[4]++;
      }
   }
   cout << "Stats: \n";
   cout << "\033[93m+\033[0m Open: " << stats[0] << endl;
   cout << "\033[96m-\033[0m Closed: " << stats[1] << endl;
   cout << "\033[95mP\033[0m Path: " << stats[2] << endl;
   cout << "\u001b[44mX\033[0m Walls: " << stats[3] << endl;
   cout << "Total spaces: " << stats[4] << endl << endl;
}

Cell Grid::getStart()
{
   return m_start;
}

Cell Grid::getEnd()
{
   return m_end;
}

char Grid::getCell(Cell location)
{
   return m_grid[location.x][location.y];
}

vector<Cell> Grid::getNeighbors(Cell location)
{
   vector<Cell> neighbors;
   if(location.x > 0)
   {
      neighbors.push_back({ location.x - 1, location.y, m_grid[location.x - 1][location.y] });
   }
   if(location.x < m_width - 1)
   {
      neighbors.push_back({ location.x + 1, location.y, m_grid[location.x + 1][location.y] });
   }
   if(location.y > 0)
   {
      neighbors.push_back({ location.x, location.y - 1, m_grid[location.x][location.y - 1] });
   }
   if(location.y < m_height - 1)
   {
      neighbors.push_back({ location.x, location.y + 1, m_grid[location.x][location.y + 1] });
   }
   return neighbors;
}

void Grid::setCell(Cell location, char value)
{
   m_grid[location.x][location.y] = value;
}

void Grid::setPath(State * finalState)
{
   State * current = finalState;
   while(current != nullptr)
   {
      if(current->getCell().value != START && current->getCell().value != END)
      {
         setCell(current->getCell(), PATH);
      }
      current = current->getParent();
   }
}