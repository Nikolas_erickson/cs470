#include <math.h>
#include <queue>
#include <iostream>

#include "grid.h"
#include "state.h"
#include "cell.h"
#include "pfBFS.h"

using namespace std;

#define OPEN '+'
#define CLOSED '-'
#define PATH 'P'
#define WALL 'X'
#define START 'S'
#define END 'E'



State * bfs(Grid & field)
{
   // create a new state for start point
   Cell startCell(field.getStart());
   Cell endCell(field.getEnd());
   State start(startCell, 0, 0);
   start.setParent(nullptr);

   queue<State *> openList;
   openList.push(&start);

   // loop until queue is empty or end point is found
   while(!openList.empty())
   {
      // get the next state from the queue
      State * currentState = openList.front();
      openList.pop();

      // mark the current state as closed
      if(currentState->getCell().value != START && currentState->getCell().value != END)
      {
         field.setCell(currentState->getCell(), CLOSED);
      }

      // get the neighbors of the current state
      vector<Cell> neighbors = field.getNeighbors(currentState->getCell());

      // for each child state of the current state
      while(!neighbors.empty())
      {
         Cell neighbor = neighbors.back();
         neighbors.pop_back();

         // if the child state is the end point
         if(neighbor.value == END)
         {
            return currentState;
         }
         else
         {
            // add the child state to the list of states to check if it is not in the open or closed list
            if(neighbor.value != OPEN && neighbor.value != CLOSED && neighbor.value != WALL && neighbor.value != START)
            {
               neighbor.value = OPEN;
               field.setCell(neighbor, OPEN);
               State * child = new State(neighbor, currentState->getCost() + 1, 0);
               child->setParent(currentState);
               openList.push(child);
            }
         }
      }
   }
   cout << "No path found\n";
   return nullptr;
}