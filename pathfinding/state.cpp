#include "state.h"
#include "cell.h"

State::State()
{
   m_location = { 0, 0, ' ' };
   m_cost = 0;
   m_heuristic = 0;
   m_parent = nullptr;
}

State::State(int x, int y, int cost, int heuristic)
{
   m_location = { x, y, ' ' };
   m_cost = cost;
   m_heuristic = heuristic;
   m_parent = nullptr;
}

State::State(Cell location, int cost, int heuristic)
{
   m_location = location;
   m_cost = cost;
   m_heuristic = heuristic;
   m_parent = nullptr;
}

State::~State()
{
}
