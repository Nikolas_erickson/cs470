#include <math.h>
#include <queue>
#include <iostream>
#include <unordered_map>

#include "grid.h"
#include "state.h"
#include "cell.h"
#include "aStarManhattan.h"

#define OPEN '+'
#define CLOSED '-'
#define PATH 'P'
#define WALL 'X'
#define START 'S'
#define END 'E'


using namespace std;

struct ASMGreaterCost
{
   bool operator()(State * a, State * b) const
   {
      return (a->getCost() + a->getHeuristic()) > (b->getCost() + b->getHeuristic());
   }
};

static int hashCell(Cell cell, Grid & field)
{
   return cell.x * field.getHeight() + cell.y;
}

// adds the open and closed list to the field for visualization before returning the path
static void drawField(Grid & field, unordered_map<int, bool> & closedList, priority_queue<State *, vector<State *>, ASMGreaterCost> & openList)
{
   for(int i = 0; i < field.getWidth(); i++)
   {
      for(int j = 0; j < field.getHeight(); j++)
      {
         if(closedList[hashCell({ i, j, field.getCell({ i, j , ' '}) }, field)] && field.getCell({ i, j, ' ' }) != START && field.getCell({ i, j, ' ' }) != END)
         {
            field.setCell({ i, j, CLOSED }, CLOSED);
         }
      }
   }
   while(!openList.empty())
   {
      State * currentState = openList.top();
      openList.pop();
      if(currentState->getCell().value != START && currentState->getCell().value != END && field.getCell(currentState->getCell()) != CLOSED)
      {
         field.setCell(currentState->getCell(), OPEN);
      }
   }
}

// Manhattan distance heuristic
static int calcHeuristicManhattan(Cell current, Cell end)
{
   return abs(current.x - end.x) + abs(current.y - end.y);
}

//Euclidean distance heuristic
static int calcHeuristicEuclidean(Cell current, Cell end)
{
   return sqrt(pow(current.x - end.x, 2) + pow(current.y - end.y, 2));
}

State * aStarManhattanSearch(Grid & field)
{
   // create a new state for start point
   Cell startCell(field.getStart());
   Cell endCell(field.getEnd());
   State start(startCell, 0, calcHeuristicManhattan(startCell, endCell));
   start.setParent(nullptr);

   priority_queue<State *, vector<State *>, ASMGreaterCost> openList;
   openList.push(&start);

   unordered_map<int, bool> closedList;

   // loop until queue is empty or end point is found
   while(!openList.empty())
   {
      // get the next state from the queue
      State * currentState = openList.top();
      openList.pop();

      // check if the current state is the end point
      if(currentState->getCell().value == END)
      {
         drawField(field, closedList, openList);
         return currentState;
      }

      // skip cells that were closed via shorter routes
      if(closedList[hashCell(currentState->getCell(), field)])
      {
         continue;
      }


      // mark the current state as closed
      if(currentState->getCell().value != START && currentState->getCell().value != END)
      {
         field.setCell(currentState->getCell(), CLOSED);
         closedList[hashCell(currentState->getCell(), field)] = true;
      }

      // get the neighbors of the current state
      vector<Cell> neighbors = field.getNeighbors(currentState->getCell());

      // for each child state of the current state
      while(!neighbors.empty())
      {
         Cell neighbor = neighbors.back();
         neighbors.pop_back();

         // add the child state to the list of states to check if it is not in the open or closed list, not a wall
         if(!closedList[hashCell(neighbor, field)] && neighbor.value != WALL)
         {


            State * child = new State(neighbor, currentState->getCost() + 1, calcHeuristicManhattan(neighbor, endCell));
            child->setParent(currentState);
            openList.push(child);
         }
      }
   }
   cout << "No path found\n";
   return nullptr;
}