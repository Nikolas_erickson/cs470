#ifndef greedyBestFirst_h
#define greedyBestFirst_h

#include "grid.h"
#include "state.h"

State * greedyBestSearch(Grid & field);


#endif