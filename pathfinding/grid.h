#ifndef grid_h
#define grid_h

#include <vector>

#include "state.h"
#include "cell.h"

using namespace std;



class Grid
{
private:
   int m_width;
   int m_height;
   char ** m_grid;
   Cell m_start;
   Cell m_end;

public:
   Grid();
   Grid(const char * filename);
   Grid(Grid & other);
   ~Grid();
   void load(const char * filename);
   void print();
   void printStats();

   void setPath(State * finalState);

   Cell getStart();
   Cell getEnd();
   char getCell(Cell location);
   void setCell(Cell location, char value);
   vector<Cell> getNeighbors(Cell location);

   int getWidth() { return m_width; }
   int getHeight() { return m_height; }


};

#endif