#ifndef State_H
#define State_H

#include "cell.h"

class State
{
private:
   State * m_parent = nullptr;
   Cell m_location;
   int m_cost = 0;
   int m_heuristic = 0;


public:
   Cell getCell() { return m_location; }
   int getCost() { return m_cost; }
   int getHeuristic() { return m_heuristic; }
   State();
   State(int x, int y, int cost, int heuristic);
   State(Cell location, int cost, int heuristic);
   ~State();

   void setParent(State * parent) { m_parent = parent; }
   State * getParent() { return m_parent; }
};


#endif