#ifndef cell_h
#define cell_h

struct Cell
{
   int x;
   int y;
   char value;
   Cell() : x(0), y(0), value(' ') {}
   Cell(int x, int y, char value) : x(x), y(y), value(value) {}
   bool operator==(const Cell & other) const
   {
      return x == other.x && y == other.y && value == other.value;
   }
};


#endif