#ifndef lowestCost_h
#define lowestCost_h

#include "grid.h"
#include "state.h"

State * lowestCostSearch(Grid & field);


#endif