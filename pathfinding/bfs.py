from collections import deque

def read_grid_from_file(file_path):
    with open(file_path, 'r') as file:
        grid = [list(line.strip()) for line in file.readlines()]
    return grid

def find_start_end(grid):
    start = end = None
    for r, row in enumerate(grid):
        for c, value in enumerate(row):
            if value == 'S':
                start = (r, c)
            elif value == 'E':
                end = (r, c)
    print(start, end)
    return start, end

def bfs(grid, start, end):
    rows, cols = len(grid), len(grid[0])
    queue = deque([(start, [])])
    visited = set([start])

    directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    while queue:
        # Update grid with current fringe list
        for r, c in queue:
            if grid[r][c] not in ['S', 'E']:
                grid[r][c] = '+'

        (current, path) = queue.popleft()
        r, c = current

        # Update grid with current visited cell
        if grid[r][c] not in ['S', 'E']:
            grid[r][c] = '*'

        # Print grid at each step
        print_grid(grid)

        if current == end:
            return path + [current]

        for dr, dc in directions:
            nr, nc = r + dr, c + dc

            if 0 <= nr < rows and 0 <= nc < cols and (nr, nc) not in visited:
                if grid[nr][nc] == '0' or grid[nr][nc] == 'E':
                    visited.add((nr, nc))
                    queue.append(((nr, nc), path + [current]))

    return None

def main(file_path):
    grid = read_grid_from_file(file_path)
    start, end = find_start_end(grid)

    if start is None or end is None:
        print("Start or end position not found in the grid.")
        return

    path = bfs(grid, start, end)

    if path is None:
        print("No path found from start to end.")
    else:
        for r, c in path:
            if grid[r][c] != 'S' and grid[r][c] != 'E':
                grid[r][c] = '.'

        for row in grid:
            print(''.join(row))
        for a in path:
            print(repr(a))
        print("path length: "+repr(len(path)))

if __name__ == '__main__':
    file_path = 'grid.txt'  # Replace with your file path
    main(file_path)
