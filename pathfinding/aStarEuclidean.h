#ifndef aStarEuclidean_h
#define aStarEuclidean_h

#include "grid.h"
#include "state.h"

State * aStarEuclideanSearch(Grid & field);


#endif