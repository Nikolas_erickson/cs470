#include <iostream>
#include <vector>
#include <fstream>
#include <string>


#define COLUMNS 7
#define ROWS 6

#include "Board.h"

using namespace std;

Board::Board()
{
   grid = new char[COLUMNS * ROWS];
   for(int i = 0; i < COLUMNS; i++)
   {
      for(int j = 0; j < ROWS; j++)
      {
         grid[i * ROWS + j] = ' ';
      }
   }
}

// copy constructor implements deep copy
Board::Board(const Board & other)
{
   if(other.grid == nullptr)
   {
      grid = nullptr;
      return;
   }
   grid = new char[COLUMNS * ROWS];
   for(int i = 0; i < COLUMNS; i++)
   {
      for(int j = 0; j < ROWS; j++)
      {
         grid[i * ROWS + j] = other.grid[i * ROWS + j];
      }
   }
}

Board::~Board()
{
   if(grid)
   {
      delete[] grid;
   }
}

void Board::loadBoard(const char * filename)
{
   ifstream file;
   file.open(filename);
   if(file.is_open())
   {
      for(int j = 0; j < ROWS; j++)
      {
         for(int i = 0; i < COLUMNS; i++)
         {
            char c = file.get();
            while(c != ' ' && c != 'X' && c != 'O')
            {
               c = file.get();
            }
            grid[i * ROWS + j] = c;
         }
      }
   }
   file.close();
}

void Board::print() const
{
   for(int j = 0; j < ROWS; j++)
   {
      for(int i = 0; i < COLUMNS; i++)
      {
         std::cout << "|";
         switch(grid[i * ROWS + j])
         {
         case ' ':
            std::cout << " ";
            break;
         case 'X':
            std::cout << "\u001b[43mX\u001b[40m";
            break;
         case 'O':
            std::cout << "\u001b[41mO\u001b[40m";
            break;
         }
      }
      std::cout << "|\n";
   }
   for(int i = 0; i < COLUMNS; i++)
   {
      std::cout << "--";
   }
   std::cout << "-\n";
   for(int i = 0; i < COLUMNS; i++)
   {
      std::cout << " " << i;
   }
   std::cout << "\n";
}
bool Board::makeMove(int column, char piece)
{
   if(column < 0 || column >= COLUMNS)
   {
      return false;
   }
   for(int i = ROWS - 1; i >= 0; i--)
   {
      if(grid[column * ROWS + i] == ' ')
      {
         grid[(column) *ROWS + (i)] = piece;
         return true;
      }
   }
   return false;
}

bool Board::isValidMove(int column) const
{
   if(column < 0 || column >= COLUMNS)
   {
      return false;
   }
   if(grid[(column) *ROWS + (0)] == ' ')
   {
      return true;
   }
   return false;
}

bool Board::isFull() const
{
   for(int i = 0; i < COLUMNS; i++)
   {
      if(grid[(i) *ROWS] == ' ')
      {
         return false;
      }
   }
   return true;
}

bool Board::checkWin(char piece) const
{
   for(int i = 0; i < COLUMNS; i++)
   {
      for(int j = 0; j < ROWS; j++)
      {
         if(grid[(i) *ROWS + (j)] == piece)
         {
            if(checkVertical(i, j, piece) || checkHorizontal(i, j, piece) || checkDiagonal(i, j, piece))
            {
               return true;
            }
         }
      }
   }
   return false;
}

bool Board::checkVertical(int row, int column, char piece) const
{
   if(row + 3 < COLUMNS)
   {
      for(int i = 1; i < 4; i++)
      {
         if(grid[(row + i) * ROWS + (column)] != piece)
         {
            return false;
         }
      }
      return true;
   }
   return false;
}

bool Board::checkHorizontal(int row, int column, char piece) const
{
   if(column + 3 < ROWS)
   {
      for(int i = 1; i < 4; i++)
      {
         if(grid[(row) *ROWS + (column + i)] != piece)
         {
            return false;
         }
      }
      return true;
   }
   return false;
}

bool Board::checkDiagonal(int row, int column, char piece) const
{
   if(row + 3 < COLUMNS && column + 3 < ROWS)
   {
      for(int i = 1; i < 4; i++)
      {
         if(grid[(row + i) * ROWS + (column + i)] != piece)
         {
            return false;
         }
      }
      return true;
   }
   if(row + 3 < COLUMNS && column - 3 >= 0)
   {
      for(int i = 1; i < 4; i++)
      {
         if(grid[(row + i) * ROWS + (column - i)] != piece)
         {
            return false;
         }
      }
      return true;
   }
   return false;
}

string Board::getHash() const
{
   string hash = "";
   for(int j = 0; j < ROWS; j++)
   {
      for(int i = 0; i < COLUMNS; i++)
      {
         hash += grid[(i) *ROWS + (j)];
      }
   }
   return hash;
}

string Board::getHashMirror() const
{
   string hash = "";
   for(int j = 0; j < ROWS; j++)
   {
      for(int i = COLUMNS - 1; i >= 0; i--)
      {
         hash += grid[(i) *ROWS + (j)];
      }
   }
   return hash;
}


int Board::evaluate(char piece, int * pointValues) const
{
   int value = 0;

   //int pointValues[4] = { 100, 3, 2, 1 };
   // point values represent values for certain situations
   // value[0] represents a possible win position
   //  - a possible win position is a position where a player has atleast 4 spaces in a row made up of empties and player pieces
   //  - the value of a possible win position is 100 * the number of pieces in the position * number of spaces greater than 4
   // value[1] represents the multiplier for a player piece in a row/column/diagonal of spaces
   // value[2] represents the multiplier for an empty space in a r/c/d of spaces
   // value[3] represents the initial point value in a r/c/d of spaces
   //  - example: if a player has OX XOX, the value of that row is v[3]*v[1]*v[2]*v[1] + v[3]*v[1]


   /*
   for(int i = 0; i < COLUMNS; i++)
   {
      for(int j = 0; j < ROWS; j++)
      {
         if(grid[(i)*ROWS+(j)] == piece)
         {
            value += 10 * i;
         }
         else if(grid[(i)*ROWS+(j)] != ' ')
         {
         }
      }
   }
   return value;*/




   // check for potential horizontal wins
   for(int i = 0; i < 6; i++) // for each row
   {
      int rowValue = pointValues[3];
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      for(int j = 0; j < 8; j++)
      {
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            //value += 100 * piecesInPosition;
            break;
         }
         if(j == 7)
         {
            value += rowValue;
            break;
         }
         possibleWinPosition++;
         if(grid[(j) *ROWS + (i)] == piece)
         {
            rowValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(j) *ROWS + (i)] == ' ')
         {
            rowValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            value += rowValue;
            rowValue = pointValues[3];
         }
      }
   }

   // check for potential vertical wins
   for(int i = 6; i <= 0; i++) // for each column
   {
      int columnValue = pointValues[3];
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      for(int j = 0; j < 7; j++)
      {
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            break;
         }
         if(j == 6)
         {
            value += columnValue;
            break;
         }
         possibleWinPosition++;
         if(grid[(j) *ROWS + (i)] == piece)
         {
            columnValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(j) *ROWS + (i)] == ' ')
         {
            columnValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            value += columnValue;
            columnValue = pointValues[3];
         }
      }
   }


   // check for potential up left diagonal wins
   for(int i = 3; i < 7; i++)
   {
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      int diagonalValue = pointValues[3];
      for(int k = 0; k < 4 + (i - 3) && k < 6; k++)
      {
         possibleWinPosition++;
         if(grid[(i - k) * ROWS + (5 - k)] == piece)
         {
            diagonalValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(i - k) * ROWS + (5 - k)] == ' ')
         {
            diagonalValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            diagonalValue = pointValues[3];
         }
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            break;
         }
      }
      value += diagonalValue;
   }
   for(int j = 3; j < 5; j++)
   {
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      int diagonalValue = pointValues[3];
      for(int k = 0; k < 4 + (j - 3); k++)
      {
         possibleWinPosition++;
         if(grid[(6 - k) * ROWS + (j - k)] == piece)
         {
            diagonalValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(6 - k) * ROWS + (j - k)] == ' ')
         {
            diagonalValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            diagonalValue = pointValues[3];
         }
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            break;
         }
      }
      value += diagonalValue;
   }


   // check for potential up right diagonal wins
   for(int i = 3; i >= 0; i--)
   {
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      int diagonalValue = pointValues[3];
      for(int k = 0; k < 4 + (3 - i) && k < 6; k++)
      {
         possibleWinPosition++;
         if(grid[(i + k) * ROWS + (5 - k)] == piece)
         {
            diagonalValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(i + k) * ROWS + (5 - k)] == ' ')
         {
            diagonalValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            diagonalValue = pointValues[3];
         }
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            break;
         }
      }
      value += diagonalValue;
   }
   for(int j = 3; j < 5; j++)
   {
      int possibleWinPosition = 0;
      int piecesInPosition = 0;
      int diagonalValue = pointValues[3];
      for(int k = 0; k < 4 + (j - 3); k++)
      {
         possibleWinPosition++;
         if(grid[(k) *ROWS + (j - k)] == piece)
         {
            diagonalValue *= pointValues[1];
            piecesInPosition++;
         }
         else if(grid[(k) *ROWS + (j - k)] == ' ')
         {
            diagonalValue *= pointValues[2];
         }
         else
         {
            possibleWinPosition = 0;
            piecesInPosition = 0;
            diagonalValue = pointValues[3];
         }
         if(possibleWinPosition >= 4)
         {
            value += pointValues[0] * piecesInPosition;
            break;
         }
      }
      value += diagonalValue;
   }



   return value;

}