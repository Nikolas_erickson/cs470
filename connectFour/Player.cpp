#include <iostream>
#include <vector>
#include <chrono>
#include <unordered_map>
#include <string>

#include "Player.h"

#define INF 999999999
#define DEPTHOFSEARCH 12

#define DEBUG true

using namespace std;

Player::Player(char piece, bool isHuman)
{
   this->m_piece = piece;
   this->m_isHuman = isHuman;
   this->m_weights = nullptr;
}
Player::Player(char piece, bool isHuman, int * weights)
{
   this->m_piece = piece;
   this->m_isHuman = false;
   this->m_weights = new int[4];
   for(int i = 0; i < 4; i++)
   {
      this->m_weights[i] = weights[i];
   }
}
Player::~Player()
{
}
void Player::setPointValues(int * weights)
{
   if(this->m_weights)
   {
      delete[] this->m_weights;
   }
   this->m_weights = weights;
}

bool Player::getPointValues(int * weights) const
{
   if(this->m_weights == nullptr)
   {
      return false;
   }
   for(int i = 0; i < 4; i++)
   {
      weights[i] = this->m_weights[i];
   }
   return true;
}

char Player::getPiece() const
{
   return m_piece;
}

bool Player::isHuman() const
{
   return m_isHuman;
}

int Player::getMove(Board board) const
{
   if(m_isHuman)
   {
      int column;
      cout << "Enter a column number (0-6): ";
      do
      {
         cin >> column;
         //input validation
         if(cin.fail())
         {
            cin.clear();
            cin.ignore(1000, '\n');
            column = -1;
         }
         if(column >= 90)
         {
            cout << board.isValidMove(column - 90) << endl;
         }
      }
      while(!board.isValidMove(column));
      return column;
   }
   else
   {
      return getBestMove(board);
   }
}

// get time for debugging
uint64_t millis()
{
   uint64_t ms = chrono::duration_cast<chrono::milliseconds>(
      chrono::high_resolution_clock::now().time_since_epoch())
      .count();
   return ms;
}

int minimax(Board board, int depth, int alpha, int beta, bool maxi, char player, char opponent, long movestr, unordered_map<string, int> & hashTable, int * dna)
{
   if(hashTable.find(board.getHash()) != hashTable.end())
   {
      return hashTable[board.getHash()];
   }
   if(hashTable.find(board.getHashMirror()) != hashTable.end())
   {
      //return hashTable[board.getHashMirror()];
   }
   // terminal conditions
   if(depth == 0 || board.checkWin(opponent) || board.isFull())
   {
      int multiplier = (maxi) ? 1 : -1;
      // return value of board
      if(board.checkWin(opponent))
      {

         return multiplier * 100000 * (depth + 1);
      }
      else
      {
         return multiplier * (board.evaluate(opponent, dna) - board.evaluate(player, dna));
      }
   }

   // for each possible move
   int bestValue;
   int moves[7] = { 3,4,2,5,1,6,0 };
   if(maxi)
   {
      bestValue = INF;
   }
   else
   {
      bestValue = -INF;
   }
   for(int i = 0; i < 7; i++)
   {
      if(board.isValidMove(moves[i]))
      {
         int value;
         // make new board with that move
         Board newBoard = Board(board);
         newBoard.makeMove(moves[i], player);
         // value = -negamax
         if(maxi)
         {
            value = minimax(newBoard, depth - 1, alpha, beta, false, opponent, player, movestr * 10 + moves[i], hashTable, dna);
            bestValue = min(bestValue, value);
            beta = min(beta, value);
            if(value <= alpha)
            {
               break;
            }
         }
         else
         {
            value = minimax(newBoard, depth - 1, alpha, beta, true, opponent, player, movestr * 10 + moves[i], hashTable, dna);
            bestValue = max(bestValue, value);
            alpha = max(alpha, value);
            if(value >= beta)
            {
               break;
            }
         }
         //DEBUGGING
         // 1 in 10000 chance to print the board and value and movestr
         bool printRandMoves = false;
         if(printRandMoves && rand() % 1 == 0)
         {
            cout << "Move: " << moves[i] << " Value: " << value << " Movestr: " << movestr * 10 + moves[i] << endl;
            newBoard.print();
         }
      }
   }

   // return bestValue
   hashTable[board.getHash()] = bestValue;
   return bestValue;

}

vector<int> aiTimes;

int Player::getBestMove(Board board) const
{
   char opponent = m_piece == 'X' ? 'O' : 'X';
   // get current AI evaluation weights
   int dna[4];
   getPointValues(dna);
   //DEBUGGING
   if(DEBUG) cout << "Calculating best move..." << endl;
   uint64_t time = millis();
   //DEBUGGING

   // for each possible move
   int moves[7] = { 3,4,2,5,1,6,0 };
   int values[7] = { -INF, -INF, -INF, -INF, -INF, -INF, -INF };
   int alpha = -INF;
   unordered_map<string, int> hashTable;
   for(int i = 0; i < 7; i++)
   {
      if(board.isValidMove(moves[i]))
      {
         // make new board with that move
         Board newBoard = Board(board);
         newBoard.makeMove(moves[i], m_piece);
         //newBoard.print();
         // value = negamax(new board, depth, maxDepth)
         values[i] = minimax(newBoard, DEPTHOFSEARCH, alpha, INF, true, opponent, m_piece, moves[i], hashTable, dna);
         if(values[i] > alpha)
         {
            alpha = values[i];
         }
         //cout << "Move: " << moves[i] << " Value: " << values[i] << endl;
         //cout << "******************" << endl;
      }
   }

   //DEBUGGING
   if(DEBUG)
   {
      cout << "Time: " << millis() - time << "ms" << endl;
      aiTimes.push_back(millis() - time);
      //display average time and max time
      int sum = 0;
      int max = 0;
      for(int i = 0; i < aiTimes.size(); i++)
      {
         sum += aiTimes[i];
         if(aiTimes[i] > max)
         {
            max = aiTimes[i];
         }
      }
      cout << "Average time: " << sum / aiTimes.size() << "ms" << endl;
      cout << "Max time: " << max << "ms" << endl;
   }
   //DEBUGGING

   // return move with highest value
   int bestValue = -INF;
   int bestIndex = 0;
   for(int i = 0; i < 7; i++)
   {
      //cout << "Move: " << moves[i] << " Value: " << values[i] << endl;
      if(values[i] > bestValue)
      {
         bestValue = values[i];
         bestIndex = i;
      }
   }
   if(DEBUG) cout << "----------------------" << endl;
   return moves[bestIndex];


   return 0;
}