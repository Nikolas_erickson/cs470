#include <iostream>
#include <vector>

#include "Game.h"

using namespace std;

Game::Game()
{
   gameOver = false;
   currentPlayer = 0;
   initPlayers();
}
Game::Game(Player player1, Player player2)
{
   gameOver = false;
   currentPlayer = 0;
   players.push_back(player1);
   players.push_back(player2);
}

int Game::play(bool quiet)
{
   while(!gameOver)
   {
      if(!quiet)
      {
         printBoard();
         printCurrentPlayer();
      }
      makeMove();
      updateGameStatus();
      switchPlayer();
   }
   switchPlayer(); // switch player back to winning player if game ends
   printBoard();
   return printWinner();
}

void Game::initPlayers()
{
   cout << "Choose one of the following options: " << endl;
   cout << "1. Player vs Player" << endl;
   cout << "2. Player vs Computer" << endl;
   cout << "3. Computer vs Player" << endl;
   cout << "4. Computer vs Computer" << endl;
   int option;
   cin >> option;
   // handle input validation
   if(cin.fail())
   {
      cin.clear();
      cin.ignore(1000, '\n');
      option = 1;
   }

   if(option == 1)
   {
      players.push_back(Player('X'));
      players.push_back(Player('O'));
   }
   else if(option == 2)
   {
      players.push_back(Player('X'));
      players.push_back(Player('O', false, new int[4] {100, 3, 2, 1}));
   }
   else if(option == 3)
   {
      players.push_back(Player('X', false, new int[4] {100, 3, 2, 1}));
      players.push_back(Player('O'));
   }
   else if(option == 4)
   {
      players.push_back(Player('X', false, new int[4] {100, 3, 2, 1}));
      players.push_back(Player('O', false, new int[4] {100, 3, 2, 1}));
   }
   else if(option == 5)
   {
      players.push_back(Player('X'));
      players.push_back(Player('O', false, new int[4] {100, 3, 2, 1}));
      board.loadBoard("state.txt");
   }
   else if(option == 8)
   {
      players.push_back(Player('X', false, new int[4] {100, 3, 2, 1}));
      players.push_back(Player('O', false, new int[4] {252, 5, 9, 4}));
   }
   else if(option == 9)
   {
      players.push_back(Player('O', false, new int[4] {252, 5, 9, 4}));
      players.push_back(Player('X', false, new int[4] {100, 3, 2, 1}));
   }
   else
   {
      cout << "Invalid option. Defaulting to Player vs Player." << endl;
      players.push_back(Player('X'));
      players.push_back(Player('O'));
   }
}

void Game::switchPlayer()
{
   currentPlayer = (currentPlayer + 1) % players.size();
}

void Game::printBoard()
{
   board.print();
}

int Game::printWinner()
{
   int result = 0;
   if(board.checkWin(players[currentPlayer].getPiece()))
   {
      result = currentPlayer + 1;
   }
   else
   {
      printTie();
   }
   return result;
}

void Game::printTie()
{
   if(board.isFull())
   {
      std::cout << "It's a tie!" << std::endl;
   }
}

void Game::printCurrentPlayer()
{
   std::cout << "Player " << players[currentPlayer].getPiece() << "'s turn." << std::endl;
}

void Game::makeMove()
{
   int column;
   column = players[currentPlayer].getMove(board);
   board.makeMove(column, players[currentPlayer].getPiece());
}

void Game::updateGameStatus()
{
   if(board.checkWin(players[currentPlayer].getPiece()))
   {
      gameOver = true;
      cout << "\u001b[44m" << players[currentPlayer].getPiece() << " wins!\u001b[40m" << endl;
   }
   else if(board.isFull())
   {
      gameOver = true;
      cout << "\u001b[44mIt's a tie!\u001b[40m" << endl;
   }
}

